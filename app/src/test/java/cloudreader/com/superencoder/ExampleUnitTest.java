package cloudreader.com.superencoder;

import org.junit.Test;

import cloudreader.com.superencoder.helpers.FFMPEGCommandGenerator;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void FullVideoConversionCommandGeneratePerfectly() {
        FFMPEGCommandGenerator generator = new FFMPEGCommandGenerator();
        String output = generator.generateCommand("abc.mp4", "abcd", "mp4");
        assertEquals(output, "-i abc.mp4 abcd.mp4");
    }

    @Test
    public void PartialVideoConversionCommandGeneratePerfectly() {
        FFMPEGCommandGenerator generator = new FFMPEGCommandGenerator();
        String output = generator.generateCommand("abc.mp4", "abcd", "mp4", 30, 10);
        assertEquals(output, "-ss 30 -i abc.mp4 -t 10 -c copy abcd.mp4");
    }

    @Test
    public void VideoConversionToGifCommandGeneratePerfectly() {
        FFMPEGCommandGenerator generator = new FFMPEGCommandGenerator();
        String output = generator.generateCommand("abc.mp4", "abcd", "GIF", 30, 10);
        assertEquals(output, "-ss 30 -i abc.mp4 -pix_fmt rgb24 -r 10 -s 320x240 -t 10 abcd.gif");
    }
}