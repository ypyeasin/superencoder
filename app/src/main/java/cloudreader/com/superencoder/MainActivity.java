package cloudreader.com.superencoder;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;

import cloudreader.com.superencoder.adapter.ImageListAdapter;
import cloudreader.com.superencoder.helpers.AddHelper;

public class MainActivity extends AppCompatActivity {

    GridView gv;
    public static final String VIDEO_PARAM = "videoPath";
    LinearLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //  initiating the Add
        mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        AddHelper.setBannerAdd( (AdView) findViewById(R.id.adView), mainLayout, R.id.adView);

        //  Generating grid View
        generateGridView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        generateGridView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.reload:
                generateGridView();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    *   Internal Methods
    */

    //  Generates the grid view
    private void generateGridView() {
        Log.d("GRID VIEW", "GRID VIEW GRID VIEW GRID VIEW GRID VIEW GRID VIEW GRID VIEW ");
        gv = (GridView) findViewById(R.id.gridView);
        gv.setAdapter(new ImageListAdapter(MainActivity.this, getVideoList()));
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                openEditPanel(item);
            }

        });
    }

    //  Opens the Edit video screen
    private void openEditPanel(String videoPath) {
        Intent intent = new Intent(MainActivity.this, EditActivity.class);
        intent.putExtra(MainActivity.VIDEO_PARAM, videoPath);
        startActivity(intent);
    }

    //  Returns a String Array with the list of video directory
    private String[] getVideoList() {
        List<String> videoList = new ArrayList<String>();
        Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Video.VideoColumns.DATA};
        Cursor c = this.getContentResolver().query(uri, projection, null, null, MediaStore.Video.VideoColumns.DATE_TAKEN + " DESC");

        if (c != null) {
            while (c.moveToNext()) {
                videoList.add(c.getString(0));
            }
            c.close();
        }

        String[] vidArr = new String[videoList.size()];
        vidArr = videoList.toArray(vidArr);

        return vidArr;
    }

}
