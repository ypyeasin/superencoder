package cloudreader.com.superencoder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cloudreader.com.superencoder.helpers.Utils;

public class ForegroundService extends Service {

    PendingIntent pendingIntent;
    String totalVideoDuration, currentPositionTime, outputPath;
    String[] timeSplits;
    Matcher patternMatcher;
    Notification mBuilder;
    int progressPercentage = 0, progressNotificationID = 001, completionNotificationID = 002;
    double totalTimeLong, currentTimeLong;
    Pattern totalTimePattern = Pattern.compile("Duration: [0-9]+:[0-9]+:[0-9]+.[0-9]+"),
            currentPositionPattern = Pattern.compile("time=[0-9]\\S+");

    public static final String CMD = "cmd",
                        VIDEO_PATH = "video_path";


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String command = intent.getStringExtra(CMD);
        outputPath = intent.getStringExtra(VIDEO_PATH);
        executeFFMPEGCommand(command);
        Log.d("---------", command);
        return START_STICKY;
    }

    /*
    *   Internal methods
    */

    //  Executes the given ffmpeg command
    private void executeFFMPEGCommand(String command) {
        FFmpeg ffmpeg = FFmpeg.getInstance(this);
        try {
            String[] cmd = command.split(" ");
            ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {}

                @Override
                public void onProgress(String message) {
                    updatePosition(message); //  Sets the value of totalVideoDuration, currentPositionTime
                    progressNotification();
                }

                @Override
                public void onFailure(String message) {
                    Log.d("FAIL======", message.toString());
                    completionNotification(getResources().getString(R.string.CONVERSION_FAILED), getResources().getString(R.string.SORRY_FAILED_TO_CONVERT), false);
                }

                @Override
                public void onSuccess(String message) {
                    completionNotification(getResources().getString(R.string.CONVERSION_COMPLETED), getResources().getString(R.string.CLICK_TO_OPEN), true);
                }

                @Override
                public void onFinish() {terminateProgressNotification();}
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            Toast.makeText(this, getResources().getString(R.string.CONVERSION_ALREADY_RUNNING), Toast.LENGTH_LONG).show();
        }
    }

    //  Updates the notification with conversion progress
    private void progressNotification() {
        progressPercentage = calculateProgressPercentage();
        mBuilder = new NotificationCompat.Builder(this)
            .setSmallIcon(R.drawable.icon)
            .setOngoing(true)
            .setContentTitle( getResources().getString(R.string.CONVERSION_PROGRESS) )
            .setContentText(progressPercentage + "%")
            .setProgress(100, progressPercentage, false)
            .build();
        startForeground(progressNotificationID, mBuilder);
    }

    //  This is used when on successful/Failure conversion to generate the notification
    private void completionNotification(String notificationTitle, String notificationText, Boolean success){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        File file = new File(outputPath);   //  "/storage/emulated/0/music/test.mp4"
        intent.setDataAndType(Uri.fromFile(file), Utils.getMIMEType(outputPath));
        pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

        NotificationCompat.Builder cBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
            .setSmallIcon(R.drawable.icon)
            .setAutoCancel(true)
            .setContentTitle(notificationTitle)
            .setContentText(notificationText);

        if( success ) cBuilder.setContentIntent(pendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(completionNotificationID, cBuilder.build());
    }

    //  Terminates the sticky progress notification
    private void terminateProgressNotification(){
        stopForeground(true);
    }

    //  Sets the value of totalVideoDuration & currentPositionTime
    private void updatePosition(String log) {
        patternMatcher = totalTimePattern.matcher(log);
        if (patternMatcher.find()) {
            timeSplits = log.trim().replace(",", "").toLowerCase().split(" ");
            totalVideoDuration = trimOutMiliSecondFromTimeStamp(timeSplits[1]);
        } else {
            patternMatcher = currentPositionPattern.matcher(log);
            if (patternMatcher.find()) {
                timeSplits = patternMatcher.group().split("=");
                currentPositionTime = trimOutMiliSecondFromTimeStamp(timeSplits[1]);
            }
        }
    }

    //  Calculates the progress of conversion in percentage.
    protected int calculateProgressPercentage() {
        totalTimeLong = (double) timeStampToSecond(totalVideoDuration);
        currentTimeLong = (double) timeStampToSecond(currentPositionTime);
        if( totalTimeLong == 0 || currentTimeLong == 0) return 0;
        Log.d("&&&&&&&&&", "Total Time " + totalTimeLong + " Current time " + currentTimeLong + " Percent % " + ((currentTimeLong/totalTimeLong)*100) );
        return (int)((currentTimeLong/totalTimeLong) * 100);
    }

    //  Converts time stamp to seconds, making the (Hour*60*60+Minute*60+Second)
    private int timeStampToSecond(String time) {
        if( null == time ) return 0;
        timeSplits = time.split(":");
        return Integer.parseInt(timeSplits[0]) * 60 *60 + Integer.parseInt(timeSplits[1]) * 60 + Integer.parseInt(timeSplits[2]);
    }

    //  Trims out the Mil-second from parameter. 00:00:00.0 would be 00:00:00
    private String trimOutMiliSecondFromTimeStamp(String timeStamp) {
        return timeStamp.replaceFirst("\\.[0-9]+", "");
    }

}
