package cloudreader.com.superencoder.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import cloudreader.com.superencoder.R;

/**
 * Created by arnab on 12/28/2016.
 */

public class ImageListAdapter extends ArrayAdapter {
    private Context context;
    private LayoutInflater inflater;

    private String[] imageUrls;

    public ImageListAdapter(Context context, String[] imageUrls) {
        super(context, R.layout.gridview_item_image, imageUrls);

        this.context = context;
        this.imageUrls = imageUrls;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.gridview_item_image, parent, false);
        }

        ColorDrawable colorPlaceHolder = new ColorDrawable(context.getResources().getColor(R.color.not_loaded_thumbnail) );

        Glide
                .with(context)
                .load(imageUrls[position])
                .centerCrop()
                .placeholder(colorPlaceHolder)
                .into((ImageView) convertView);

        return convertView;
    }
}
