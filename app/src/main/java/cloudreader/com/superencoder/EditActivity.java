package cloudreader.com.superencoder;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.util.Util;
import com.google.android.gms.ads.AdView;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import cloudreader.com.superencoder.helpers.AddHelper;
import cloudreader.com.superencoder.helpers.FFMPEGCommandGenerator;
import cloudreader.com.superencoder.helpers.Utils;

public class EditActivity extends AppCompatActivity {

    String videoPath, cmd, defaultExportOption = "mp4", exportAs = defaultExportOption;
    VideoView videoView;
    TextView startTimeTextView, endTimeTextView, currentPositionTextView, videoLengthTextView;
    SeekBar seekbar;
    RangeSeekBar<Integer> rangeSeekBar;
    public int videoLength = 0, markIn = 0, markOut = 0, swipePosition;
    Button pausePlayBtn;
    Thread seekBarThread, selectionThread;
    Spinner spinner;
    LinearLayout mainLayout, rangeSeekBarLayout, playerLayout;
    float x1, x2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
        playerLayout = (LinearLayout) findViewById(R.id.playerLayout);
        startTimeTextView = (TextView) findViewById(R.id.startTimeTextView);
        endTimeTextView = (TextView) findViewById(R.id.endTimeTextView);
        pausePlayBtn = (Button) findViewById(R.id.pausePlayBtn);
        currentPositionTextView = (TextView) findViewById(R.id.currentPosition);
        videoLengthTextView = (TextView)findViewById(R.id.videoLength);

        AddHelper.setBannerAdd( (AdView) findViewById(R.id.editAdView), mainLayout, R.id.adView);
        setVideoPath();
        initMediaPlayer();
        playerLayoutListner();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_activity_menu, menu);

        MenuItem item = menu.findItem(R.id.exportOptions);
        spinner = (Spinner) MenuItemCompat.getActionView(item);
        spinnerEventHandler();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.export:
                save(item.getActionView());
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    *   Element event handlers
    */

    //  Toggle play/pause
    public void togglePlayPause(View view) {
        if (videoView.isPlaying()) pauseVideo();
        else playVideo();
    }

    //  Plays only selected part of the video
    public void playSelectedDuration(View view) {
        updateVideoAndSeek(markIn);
        playVideo();
        startSelectionThread();
    }

    //  Sets th current position as mark in
    public void markIn(View view) {
        markIn = videoView.getCurrentPosition();
        if (markOut <= markIn && markOut != 0) return;
        String startText = Utils.mSecToHMS(markIn);
        startTimeTextView.setText(startText);

        rangeSeekBar.setSelectedMinValue(markIn);
    }

    //  Sets the current position as the mark out
    public void markOut(View view) {
        markOut = videoView.getCurrentPosition();
        if (markOut <= markIn) return;
        String endText = Utils.mSecToHMS(markOut);
        endTimeTextView.setText(endText);

        rangeSeekBar.setSelectedMaxValue(markOut);
    }

    //  starts converting the video
    public void save(View view) {
        //  Display interstitial Add
        AddHelper.setInterstitialAdd(this, getString(R.string.admob_interstitial_id));

        videoPath = Utils.renameWhiteSpacedPath(videoPath);
        String outputFileWithOutExtension = Utils.generateOutputFileName(videoPath);
        String outputFileWithExtension = Utils.generateOutputWithExtension(outputFileWithOutExtension, exportAs);
        int startFrom = markIn / 1000;
        int duration = (markOut - markIn) / 1000;
        Utils.deleteIfExist(outputFileWithExtension);
        initMediaPlayer();
        cmd = FFMPEGCommandGenerator.generateCommand(videoPath, outputFileWithOutExtension, exportAs, startFrom, duration);

        Intent service = new Intent(EditActivity.this, ForegroundService.class);
        service.putExtra(ForegroundService.CMD, cmd);
        service.putExtra(ForegroundService.VIDEO_PATH, outputFileWithExtension);
        startService(service);
    }

    /*
    *   Internal methods
    */

    //  Populates supported file formats mp3, mp4, gif etc to the exportOptions Spinner
    private void populateExportOptions() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.export_options, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    //  Retrieves the video path from intent
    private void setVideoPath() {
        Intent intent = getIntent();
        videoPath = intent.getStringExtra(MainActivity.VIDEO_PARAM);
    }

    //  Initializes the media player
    private void initMediaPlayer() {
        videoView = (VideoView) findViewById(R.id.videoView1);
        videoView.requestFocus();
        videoView.setVideoPath(videoPath);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoLength = videoView.getDuration();
                markOut = videoLength;
                videoView.seekTo(0);
                videoLengthTextView.setText(Utils.mSecToHMS(videoLength));
                initMarkText();
                seekBarListener();
                rangeBarListener();
            }
        });
    }

    //  Sets the swipe event for video Player
    private void playerLayoutListner(){
        playerLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent touchEvent) {
                switch (touchEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        x1 = touchEvent.getX();
                        break;
                    }
                    case MotionEvent.ACTION_MOVE: {
                        x2 = touchEvent.getX();
                        swipePosition = videoView.getCurrentPosition() + Math.round(x2 - x1) * 5;
                        updateVideoAndSeek(swipePosition);
                    }
                    case MotionEvent.ACTION_UP:{
                        x2 = touchEvent.getX();
                        swipePosition = videoView.getCurrentPosition() + Math.round(x2 - x1) * 5;
                        updateVideoAndSeek(swipePosition);
                        break;
                    }
                }
                return true;

            }
        });
    }

    //  Determines the video length & sets on videoLength
    private void initMarkText() {
        String endText = Utils.mSecToHMS(videoLength);
        startTimeTextView.setText(Utils.mSecToHMS(0));
        endTimeTextView.setText(endText);
    }

    //  Binding seekBar with handler
    private void seekBarListener() {
        seekbar = (SeekBar) findViewById(R.id.seekBar);
        seekbar.setMax(videoLength);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                currentPositionTextView.setText( Utils.mSecToHMS( seekBar.getProgress() ) );
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                videoView.pause();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                updateVideoAndSeek(seekbar.getProgress());
            }
        });
    }

    //  Binding selection rangeBar handler
    private void rangeBarListener(){
        rangeSeekBar = new RangeSeekBar<>(this);
        rangeSeekBar.setId(R.id.rangeSeekbarId);
        rangeSeekBar.setTextAboveThumbsColorResource(R.color.editor_background);
        rangeSeekBar.setRangeValues(0, videoLength);
        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                markIn = Integer.parseInt(String.valueOf(minValue));
                markOut = Integer.parseInt(String.valueOf(maxValue));

                startTimeTextView.setText( Utils.mSecToHMS(markIn) );
                endTimeTextView.setText( Utils.mSecToHMS(markOut) );
            }
        });

        rangeSeekBarLayout = (LinearLayout)findViewById(R.id.rangeSeekBarHolder);
        if( null == findViewById(R.id.rangeSeekbarId) )
            rangeSeekBarLayout.addView(rangeSeekBar);
    }

    //  Starts the video
    private void playVideo() {
        if (videoView.isPlaying()) seekBarThread.interrupt();
        videoView.start();
        startSeekBarThread();
        pausePlayBtn.setText(R.string.PAUSE_BTN_TEXT);
    }

    //  Pause/Stop the video
    private void pauseVideo() {
        videoView.pause();
        pausePlayBtn.setText(R.string.PLAY_BTN_TEXT);
        seekBarThread.interrupt();
    }

    //  starts updating the seekBar progress matching the videos's current position
    private void startSeekBarThread() {
        seekBarThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(50);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateSeekBar();
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        seekBarThread.start();
    }

    //  Starts the selection thread to find if the video has reached MarkOut position
    private void startSelectionThread() {
        if (null != selectionThread) selectionThread.interrupt();
        selectionThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(50);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                currentPositionTextView.setText(Utils.mSecToHMS(videoView.getCurrentPosition()));
                                if (videoView.getCurrentPosition() >= markOut) {
                                    selectionThread.interrupt();
                                    pauseVideo();
                                }
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        selectionThread.start();
    }

    //  updates the seekBar progress to current video position
    private void updateSeekBar() {
        seekbar.setProgress(videoView.getCurrentPosition());
        currentPositionTextView.setText(Utils.mSecToHMS(videoView.getCurrentPosition()));
    }

    //  Handles when spinner item is selected
    private void spinnerEventHandler() {
        populateExportOptions();
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                exportAs = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                exportAs = defaultExportOption;
            }
        });
    }

    //  sets the given milli-second as videoView seek & seekBar progress & current position text
    private void updateVideoAndSeek(int mSec) {
        if (mSec <= 0) mSec = 0;
        else if (mSec >= videoLength) mSec = videoLength;
        videoView.seekTo(mSec);
        seekbar.setProgress(mSec);
        currentPositionTextView.setText(Utils.mSecToHMS(mSec));
    }

}
