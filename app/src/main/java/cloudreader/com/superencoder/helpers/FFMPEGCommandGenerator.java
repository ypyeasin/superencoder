package cloudreader.com.superencoder.helpers;

/**
 * Created by Yeasin on 1/6/2017.
 */

public class FFMPEGCommandGenerator {

    public static String generateCommand(String inputFile, String outputFile, String outputFileFormat) {
        String copy = " ";

        if( Utils.isSameExtension( inputFile, outputFileFormat ) ) copy = " -c copy ";
        return "-i " + inputFile + copy + outputFile + "." + outputFileFormat.toLowerCase();
    }

    public static String generateCommand(String inputFile, String outputFile, String outputFileFormat, int startPosition, int duration) {
        String copy = " ";

        if( Utils.isSameExtension( inputFile, outputFileFormat ) ) copy = " -c copy ";

        if(outputFileFormat.equalsIgnoreCase("gif"))
            return "-ss " + startPosition + " -i " + inputFile + " -pix_fmt rgb24 -r 10 -s 320x240 -t "+ duration + " "+ outputFile + "." + outputFileFormat.toLowerCase();

        return "-ss " + startPosition + " -i " + inputFile + " -t "+ duration + copy + outputFile + "." + outputFileFormat.toLowerCase();
    }


}
