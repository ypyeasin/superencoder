package cloudreader.com.superencoder.helpers;

import android.app.Activity;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

import cloudreader.com.superencoder.R;

/**
 * Created by Yeasin on 1/6/2017.
 */

public class Utils {

    private static final String  outputPath = "/storage/emulated/0/";
    private static final String  outputDirectory = "super_encoder/";
    private static final String  outputSuffix = "_converted";

    /*
    *   Takes miliseconds as input generates corosponding HH:mm:ss format
    *   @input 1532151651
    *   @output 01:05:23
    */
    public static String mSecToHMS(int miliSeconds) {
        int sec = miliSeconds / 1000;
        Date d = new Date(sec * 1000L);
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss"); // HH for 0-23
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        return df.format(d);
    }


    /*
    *   Generates the output file name including path
    *   @output: /storage/emulated/0/music/test
    */
    public static String generateOutputFileName(String inputFilePath) {
        String[] explodedPath = inputFilePath.split("/");
        String fileNameWithExtension = explodedPath[explodedPath.length-1];
        String[] fileNameExploded = fileNameWithExtension.split("\\.");
        String newName = joiner(Arrays.copyOf(fileNameExploded, fileNameExploded.length-1), ".") + outputSuffix;

        File exportDirectory = new File(outputPath + outputDirectory);
        exportDirectory.mkdirs();

        return outputPath + outputDirectory + newName;
    }

    /*
    *   Generates the output file name including path
    */
    public static String generateOutputWithExtension(String fileName, String extension) {
        return fileName + "." + extension.toLowerCase();
    }

    /*
    *   Deletes any file conflicts with out put file
    */
    public static void deleteIfExist(String path) {
        try {
            File file = new File(path);
            if (file.exists()) file.delete();
        } catch (Exception ex) {
            Log.d("====DELETION====", "Failed to delete");
        }
    }

    /*
    *   Renames the folder or video file name if there is any space
    */
    public static String renameWhiteSpacedPath(String videoPath) {

        if (!hasWhiteSpace(videoPath)) return videoPath;

        String[] exploded = videoPath.split("/");

        for (int i = 0; i < exploded.length; i++)
            if (hasWhiteSpace(exploded[i])) exploded[i] = rename( generatePreviousPath(exploded, i), exploded[i] );

        return generatePreviousPath(exploded, exploded.length);
    }


    /*
    *   If the given media matches with given extension, it should return true
    *   @input: file path & the matching extension
    */
    public static Boolean isSameExtension(String file1, String extension) {
        String[] extension1 = file1.split("\\.");
        int length1 = extension1.length - 1;

        if (extension1[length1].equalsIgnoreCase(extension)) return true;
        return false;
    }

    /*
    *   Returns the media type based on given file path
    *   @input: /storage/0/music/test.mp4
    *   @output: video/* or audio/*
    */
    public static String getMIMEType(String filePath) {
        String result = "video/*";
        String splits[] = filePath.split("\\.");
        String extension = splits[splits.length - 1];

        if (extension.equalsIgnoreCase("mp3")) result = "audio/*";
        return result;
    }


    /*
    *   Internal methods
    */
    private static String rename(String path, String currentItem) {
        String newName = currentItem.replace(" ", "_");

        //  Implement actual renaming
        File oldFolder = new File(path, currentItem);
        File newFolder = new File(path, newName );
        oldFolder.renameTo(newFolder);

        return newName;
    }

    private static String generatePreviousPath(String[] path, int mergeUpTo) {
        String newPath = "";
        for (int i = 1; i < mergeUpTo; i++)
            newPath += "/" + path[i];
        return newPath;
    }

    //  Checks if the given file path contains any whitespace or not.
    private static Boolean hasWhiteSpace(String string) {
        String stripped = string.replace(" ", "%20");
        if (stripped.equalsIgnoreCase(string))
            return false;
        return true;
    }

    //  Takes an array of string & a joining delimeter & returns a string
    private static String joiner(String[] param, String delemiter){
        String result = "";

        for(int i = 0; i <param.length; i++){
            result += param[i];
            if( i != param.length-1 )
                result += delemiter;
        }

        return result;
    }

}
