package cloudreader.com.superencoder.helpers;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import cloudreader.com.superencoder.R;

/**
 * Created by Yeasin on 1/28/2017.
 */

public class AddHelper {

    private static InterstitialAd interstitial;

    public static void setBannerAdd(AdView adView, final LinearLayout adjustableLayout, final int reference){
        AdView mAdView = adView;
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        //  Adjusts the given layout if the add is displayed
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                param.addRule(RelativeLayout.ABOVE, reference);
                adjustableLayout.setLayoutParams(param);
            }
        });
    }

    public static void setInterstitialAdd(Context context, String interstitial_id){
        AdRequest adRequest = new AdRequest.Builder().build();

        // Prepare the Interstitial Ad
        interstitial = new InterstitialAd(context);
        // Insert the Ad Unit ID
        interstitial.setAdUnitId(interstitial_id);

        interstitial.loadAd(adRequest);
        // Prepare an Interstitial Ad Listener
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                // Call displayInterstitial() function
                if (interstitial.isLoaded()) {
                    interstitial.show();
                }
            }
        });
    }

}
